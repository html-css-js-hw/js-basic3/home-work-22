const container = document.getElementById("container");

// Створюємо таблицю та додаємо її до контейнера
const table = document.createElement("table");
for (let i = 0; i < 30; i++) {
    const row = document.createElement("tr");
    for (let j = 0; j < 30; j++) {
        const cell = document.createElement("td");
        cell.className = "cell white"; // Початковий колір - білий
        row.appendChild(cell);
    }
    table.appendChild(row);
}
container.appendChild(table);

// Функція, яка змінює колір клітинки при натисканні
function changeColor(event) {
    const cell = event.target;
    cell.classList.toggle("white");
    cell.classList.toggle("black");
}

// Додаємо обробник події click для всіх клітинок таблиці
table.addEventListener("click", changeColor);

// Додаємо обробник події click для body
document.body.addEventListener("click", function(event) {
    // Перевіряємо, чи клік був на таблиці
    if (!table.contains(event.target)) {
        // Якщо клік поза таблицею, змінюємо колір всіх клітинок одночасно
        table.classList.toggle("white");
        table.classList.toggle("black");
    }
});